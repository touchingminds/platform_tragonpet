#!/usr/bin/env python
#coding: utf-8

import axon
web = axon.web
ctx = axon.ctx
webpage = axon.webpage
cwd = axon._synapse
json = axon.json

_PLAT_URL = '/apis/tragonpet/site/index'
_PLAT_URL_ALT = '/apis/tragonpet/site'
_MOD_PATH = 'apis.tragonpet.site.index.'

urls = [
    _PLAT_URL, _MOD_PATH+'index',
    _PLAT_URL+'/', _MOD_PATH+'index',
    _PLAT_URL_ALT+'/ads', _MOD_PATH+'ads',
    _PLAT_URL_ALT+'/admin', _MOD_PATH+'admin',
    _PLAT_URL_ALT+'/admin_login', _MOD_PATH+'admin_login',
    _PLAT_URL_ALT+'/admin_logout', _MOD_PATH+'admin_logout',
    _PLAT_URL_ALT+'/stock', _MOD_PATH+'stock',
    _PLAT_URL_ALT+'/brand', _MOD_PATH+'brand',
    _PLAT_URL_ALT+'/package', _MOD_PATH+'package',
    _PLAT_URL_ALT+'/product', _MOD_PATH+'product',
    _PLAT_URL_ALT+'/edit_product', _MOD_PATH+'edit_product',
    _PLAT_URL_ALT+'/edit_product_v2', _MOD_PATH+'edit_product_v2',
    _PLAT_URL_ALT+'/coupon', _MOD_PATH+'coupon',
    _PLAT_URL_ALT+'/zipcode', _MOD_PATH+'zipcode',
    _PLAT_URL_ALT+'/ticket', _MOD_PATH+'ticket',
    _PLAT_URL_ALT+'/ad', _MOD_PATH+'ad',
    _PLAT_URL_ALT+'/user', _MOD_PATH+'user',
    _PLAT_URL+'/(css|fonts|images|js)/(.*)', _MOD_PATH+'static',
    _PLAT_URL_ALT+'/(css|fonts|images|js)/(.*)', _MOD_PATH+'static'
]

def _enc(data):
    e = data.encode('utf8').encode('hex').replace('61', 'G').replace('64', 'K')
    r = e[::-1].encode('hex')
    return r

def _dec(data):
    d = data.decode('hex')[::-1]
    r = d.replace('K', '64').replace('G', '61').decode('hex')
    return r

class admin_login:
    def POST(self):
        data = web.input()
        db_vars = {
            'username': _enc(data.username),
            'password': _enc(data.password)
        }
        user = ctx.db.query('SELECT privilege FROM tragonpet_Admin WHERE username=$username AND password=$password', db_vars)
        if not len(user):
            user = ctx.db.query('SELECT privilege FROM tm_Master WHERE username=$username AND password=$password', data)
        if len(user):
            ctx.session.login = True
            ctx.session.privilege = user[0]['privilege']
            ctx.session.username = db_vars['username']
            web.header('Content-Type','text/html; charset=utf-8', unique=True)
            js_session = 'sessionStorage.privilege = {sess_priv};'.format(sess_priv=ctx.session.privilege)
            return ''.join([
                js_session if x.startswith('//server-session') else
                x for x in webpage.get_HTML(cwd+'main.html')
            ])
        ctx.session.login = False
        web.header('Content-Type','text/html; charset=utf-8', unique=True)
        return '<html><head><script>alert("Usuario o password inválido"); location.href=document.referrer;</script></head></html>'

class admin_logout:
    def GET(self):
        ctx.session.login = False
        ctx.session.privilege = None
        ctx.session.username = None
        web.header('Content-Type','text/html; charset=utf-8', unique=True)
        return ''.join(webpage.get_HTML(cwd+'admin.html'))


class index:
    def GET(self):
        data = web.input()
        hidden_input = '<input type="hidden" id="userid" value="{userid}">'.format(userid=data.id)
        rows = ctx.db.query('SELECT * FROM tragonpet_Forgotten WHERE id=$id', {'id': data.id})
        web.header('Content-Type','text/html; charset=utf-8', unique=True)
        if len(rows):
            return ''.join([
                hidden_input if x.startswith('<!--server-input-->') else
                x for x in webpage.get_HTML(cwd+'recovery.html')
            ])
        #If no forgotten registry was found, just show the recovery page
        #Javascript disables the button by default
        else:
            return ''.join([
                x for x in webpage.get_HTML(cwd+'recovery.html')
            ])

class ads:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8')
        if ctx.session.login:
            return ''.join(webpage.get_HTML(cwd+'ads.html'))
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class user:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8')
        if ctx.session.login:
            return ''.join(webpage.get_HTML(cwd+'user.html'))
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class ad:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8')
        if ctx.session.login:
            return ''.join(webpage.get_HTML(cwd+'ad.html'))
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class edit_product_v2:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8')
        #if ctx.session.login:
        data = web.input()
        js_product_id = 'var product_id = {pid}">'.format(pid=data.id)
        
        return ''.join([
            js_product_id if x.startswith('//server-product') else
            x.decode('utf8') for x in webpage.get_HTML(cwd+'edit_product_v2.html')
        ])
        #else:
        #    return ''.join(webpage.get_HTML(cwd+'admin.html'))

class edit_product:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8')
        if ctx.session.login:
            data = web.input()
            hidden_input_id = '<input type="hidden" id="prodid" name="id" value="{pid}">'.format(pid=data.id)
            
            return ''.join([
                hidden_input_id if x.startswith('<!--server-pid-->') else
                data.name if x.startswith('<!--server-pnam-->') else
                x.decode('utf8') for x in webpage.get_HTML(cwd+'edit.html')
            ])
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class stock:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8', unique=True)
        if ctx.session.login:
            return ''.join(webpage.get_HTML(cwd+'stock.html'))
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class brand:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8', unique=True)
        if ctx.session.login:
            return ''.join(webpage.get_HTML(cwd+'brand.html'))
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class package:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8', unique=True)
        if ctx.session.login:
            return ''.join(webpage.get_HTML(cwd+'package.html'))
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class product:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8', unique=True)
        if ctx.session.login:
            return ''.join(webpage.get_HTML(cwd+'product.html'))
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class coupon:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8', unique=True)
        if ctx.session.login:
            return ''.join(webpage.get_HTML(cwd+'coupon.html'))
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class zipcode:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8', unique=True)
        if ctx.session.login:
            return ''.join(webpage.get_HTML(cwd+'zipcodes.html'))
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class ticket:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8', unique=True)
        if ctx.session.login:
            return ''.join(webpage.get_HTML(cwd+'ticket.html'))
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class admin:
    def GET(self):
        web.header('Content-Type','text/html; charset=utf-8', unique=True)
        if ctx.session.login:
            return ''.join(webpage.get_HTML(cwd+'main.html'))
        else:
            return ''.join(webpage.get_HTML(cwd+'admin.html'))

class static:
    def GET(self, media, file):
        if file.endswith('.css'):
            web.header('Content-Type','text/css; charset=utf-8', unique=True)
        if file.endswith('.js'):
            web.header('Content-Type','application/javascript; charset=utf-8', unique=True)
        if file.endswith('.gif'):
            web.header('Content-Type', 'image/gif', unique=True)
        if file.endswith('.jpg'):
            web.header('Content-Type', 'image/jpeg', unique=True)
        if file.endswith('.png'):
            web.header('Content-Type', 'image/png', unique=True)
        return ''.join(webpage.static(cwd+media, file))
