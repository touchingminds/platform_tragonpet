var mainGrid, mainToolbar, mainPopup, mainForm;
function doOnLoad() {
    //Grid
    mainGrid = new dhtmlXGridObject('the-grid');
    mainGrid.setHeader("Usuario,Tipo de Cuenta");
    mainGrid.setInitWidths("*,*");
    mainGrid.setColAlign("left,left");
    mainGrid.setColTypes("ro,ro");
    mainGrid.setColSorting("str,str");
    mainGrid.enableAutoHeight(true);
    mainGrid.init();
    
    mainToolbar = new dhtmlXToolbarObject({
        parent:"the-toolbar",
        onClick:function(id){
            if (id == 'btn-delete') {
                if (mainGrid.getSelectedRowId()) {
                    dhtmlx.confirm({
                        title: 'Eliminar usuario',
                        text: '¿Realmente desea eliminar el usuario seleccionado?',
                        ok: 'Sí, eliminar',
                        cancel: 'No',
                        callback: function(res) {
                            if (res) {
                                deleteAdminUser();
                            }
                        }
                    });
                } else {
                    dhtmlx.message({
                        text: 'Seleccione el usuario que desee eliminar'
                    });
                }
            }
        },
        icons_size: 18,
        icons_path: "css/icons/"
    });
    
    mainToolbar.addButton('btn-register', 0, 'Nueva cuenta', 'userplus.png');
    mainPopup = new dhtmlXPopup({toolbar: mainToolbar, id: 'btn-register'});
    mainForm = mainPopup.attachForm([
        {type: 'input', label: 'Usuario', name: 'uname', required: true},
        {type: 'password', label: 'Password', name: 'upass', required: true},
        {type: 'select', label: 'Tipo de cuenta', name: 'privilege', labelWidth: 130, options:[
            {text: 'Administrador', value: 1},
            {text: 'Empleado', value: 2}
        ]},
        {type: 'button', name: 'btn-create', value: 'Crear'}
    ]);
    mainToolbar.addButton('btn-delete', 1, 'Eliminar cuenta', 'userminus.png');
    getAdminUsers();
    
    //Form event
    mainForm.attachEvent('onButtonClick', function(nam) {
        if (nam == 'btn-create') {
            if (mainForm.validate()) {
                registerAdminUser();
            }
        }
    });
}

function getAdminUsers() {
    mainGrid.clearAll();
    var xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_admin_users');
    xhr.onload = function() {
        var users = JSON.parse(xhr.responseText);
        var rows = [];
        for (var i in users) {
            if (parseInt(users[i].privilege) > 1) {
                rows.push({id: i, data: [users[i].username, 'Empleado']});
            } else {
                rows.push({id: i, data: [users[i].username, 'Administrador']});
            }
        }
        mainGrid.parse({rows: rows}, 'json');
    };
    xhr.send();
}

function registerAdminUser() {
    var xhr = new XMLHttpRequest();
    var username = mainForm.getInput('uname').value;
    var password = mainForm.getInput('upass').value;
    var privilege = mainForm.getSelect('privilege').value;
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/new_admin_user');
    xhr.onload = function() {
        var r = JSON.parse(xhr.responseText);
        getAdminUsers();
        dhtmlx.message({
            text: r.message
        });
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('username='+username+'&password='+password+'&privilege='+privilege);
}

function deleteAdminUser() {
    var username = mainGrid.cells(mainGrid.getSelectedRowId(), 0).getValue();
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/delete_admin_user');
    xhr.onload = function() {
        var r = JSON.parse(xhr.responseText);
        getAdminUsers();
        dhtmlx.message({
            text: r.message
        });
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('username='+username);
}