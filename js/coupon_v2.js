let theGrid;
let theToolbar;
let packagePopup, couponForm;

function doOnLoad() {
    theToolbar = new dhtmlXToolbarObject('the_toolbar');
    theToolbar.setIconsPath("css/imgs/");
    theToolbar.addButton('popup-newcoupon', 0, 'Crear Cupón', null, null);
    theToolbar.addButton('btn-delete-package', 1, 'Eliminar Cupón', null, null);
    theToolbar.attachEvent('onClick', function(elem_id) {
        if (elem_id == 'btn-delete-package') {
            if (theGrid.getSelectedRowId()) {
                dhtmlx.confirm({
                    title: 'Eliminar cupón',
                    text: '¿Realmente desea eliminar el cupón seleccionado?',
                    ok: 'Sí, eliminar',
                    cancel: 'No',
                    callback: function(res) {
                        if (res) {
                            deleteCoupon();
                        }
                    }
                });
            } else {
                dhtmlx.message({
                    text: 'Seleccione el cupón que desea eliminar'
                });
            }
        }
    });
    
    packagePopup = new dhtmlXPopup({toolbar: theToolbar, id: 'popup-newcoupon'});
    couponForm = packagePopup.attachForm([
        {type: "input", label: "ID", name: "txt-id", required: true},
        {type: "input", label: "Descuento", name: "txt-amount", required: true},
        {type: "select", label: "Tipo de descuento", name: "sel-dsc", required: true, options: [
            {text: "Pesos ($)", value: 0},
            {text: "Porciento (%)", value: 1}
        ]},
        {type: "calendar", label: "Válido hasta", name: "expiration", required: true, enableTime: false, enableTodayButton: false},
        {type: "button", name: "btn-newcoupon", value: "Crear"}
    ]);
    
    couponForm.attachEvent('onButtonClick', function(nam) {
        if (nam == 'btn-newcoupon') {
            if (couponForm.validate()) {
                if (parseFloat(couponForm.getInput('txt-amount').value)) {
                    registerCoupon();
                } else {
                    dhtmlx.message({text: 'Cantidad inválida'});
                }
            }
        }
    });
    
    theGrid = new dhtmlXGridObject('the_grid');
    theGrid.setImagesPath("css/imgs/");
    theGrid.setHeader("ID,Descuento,Expiración");
    theGrid.setInitWidths("*,*,*");
    theGrid.setColAlign("left,left,left");
    theGrid.setColTypes("ro,ro,ro");
    theGrid.enableAutoHeight(true);
    theGrid.init();
    
    getCoupons();
}


function getCoupons() {
    let xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_coupons');
    xhr.onload = function() {
        let coupons = JSON.parse(xhr.responseText);
        let rows = [];
        for (let i in coupons) {
            let date = coupons[i].expiration.split('-').reverse().join('/');
            if (coupons[i].percentage) {
                rows.push({id: coupons[i].id, data: [coupons[i].id, coupons[i].amount+'%', date]});
            } else {
                rows.push({id: coupons[i].id, data: [coupons[i].id, '$'+coupons[i].amount, date]});
            }
        }
        theGrid.clearAll();
        theGrid.parse({rows: rows}, 'json');
        theGrid.sortRows(0, 'str', 'asc');
    };
    xhr.send();
}

function deleteCoupon() {
    let coupon_id = theGrid.getSelectedRowId();
    let xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/delete_coupon');
    xhr.onload = function() {
        let r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
        if (r.code != -1) {
            getCoupons();
        }
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('id='+coupon_id);
}


function registerCoupon() {
    let coupon_id = couponForm.getItemValue('txt-id');
    let coupon_amount = couponForm.getItemValue('txt-amount');
    let coupon_dsc = couponForm.getItemValue('sel-dsc');
    let exp_date = couponForm.getItemValue('expiration');
    
    let xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/new_coupon');
    xhr.onload = function() {
        let r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
        if (r.code != -1) {
            getCoupons();
        }
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('id='+coupon_id+'&percentage='+coupon_dsc+'&amount='+coupon_amount+'&expiration='+exp_date);
}