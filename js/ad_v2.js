var theForm;
var theVault;
var formStructure = [
   {type:"settings", position:"label-left", inputWidth: 250},
   {type: "block", list: [
      {type: "select", name:"category", label:"Tipo de anuncio:", required: true, options: [
         {value: 4, text: "Noticia"},
         {value: 5, text: "Promoción"},
         {value: 6, text: "Tip"}
      ]},
      {type: "input", name: "title", label: "Título:", tooltip: "Título del anuncio", required: true},
      {type: "input", name: "content", label: "Contenido:", tooltip: "Contenido del anuncio", rows: 5, required: true}
   ]},
   {type: "block", list: [
      {type: "button", name: "btn", value: "Guardar"},
      {type: "newcolumn"},
      {type: "button", name: "btn-back", value: "Cancelar"}
   ]}
];

function doOnLoad() {
    theForm = new dhtmlXForm('the_form', formStructure);
    theVault = new dhtmlXVaultObject({
      container:  "the_vault",
      buttonUpload: false,
      buttonClear: false,
      autoStart: false,
      filesLimit: 1,
      multiple: false
   });
    
   //Events
   theForm.attachEvent("onChange", function (name, value){
        console.log(name, value);
        if (name == 'petkind') {
            updateAge(value);
        }
   });
    
   theForm.attachEvent('onButtonClick', async function(name) {
      if (name == 'btn') {
         if (theForm.validate()) {
            if (!theVault.getStatus()) {
               dhtmlx.message({text: "Agregue una imagen"});
            } else {
               //POST form data and wait for ADID
               let adid = await uploadAd();
               
               //Set ADID to uploader
               theVault.setURL('https://www.touching-minds.com/main.py/apis/tragonpet/v2/new_ad_picture?adid='+adid);
               theVault.upload();
            }
         }
      } else if (name == 'btn-back') {
         window.history.back();
      }
   });
}

function uploadAd() {
   return new Promise(function(resolve, reject) {
      let xhr = new XMLHttpRequest();
      let data = 'kind=ad';
      data += '&category='+theForm.getItemValue('category');
      data += '&title='+theForm.getItemValue('title');
      data += '&content='+theForm.getItemValue('content');
      xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/new_ad');
      xhr.onload = function() {
         let res = JSON.parse(xhr.responseText);
         resolve(res.id);
      };
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      //xhr.send();
      console.log('Would send: '+data);
      resolve('adid');
   });
}