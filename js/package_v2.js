var theGrid;
var theToolbar;
var packagePopup, packageForm;

function doOnLoad() {
    theToolbar = new dhtmlXToolbarObject('the_toolbar');
    theToolbar.setIconsPath("css/imgs/");
    theToolbar.addButton('popup-newpackage', 0, 'Nueva Presentación', null, null);
    theToolbar.addButton('btn-delete-package', 1, 'Eliminar Presentación', null, null);
    theToolbar.attachEvent('onClick', function(elem_id) {
        if (elem_id == 'btn-delete-package') {
            if (theGrid.getSelectedRowId()) {
                dhtmlx.confirm({
                    title: 'Eliminar presentación',
                    text: '¿Realmente desea eliminar la presentación seleccionada?',
                    ok: 'Sí, eliminar',
                    cancel: 'No',
                    callback: function(res) {
                        if (res) {
                            deletePackage();
                        }
                    }
                });
            } else {
                dhtmlx.message({
                    text: 'Seleccione la presentación que desea eliminar'
                });
            }
        }
    });
    
    packagePopup = new dhtmlXPopup({toolbar: theToolbar, id: 'popup-newpackage'});
    packageForm = packagePopup.attachForm([
        {type: 'input', label: 'Nombre', name: 'txt-package', required: true},
        {type: 'input', label: 'Peso (en gramos)', name: 'txt-mass', required: true},
        {type: 'button', name: 'btn-newpackage', value: 'Agregar'}
    ]);
    
    packageForm.attachEvent('onButtonClick', function(nam) {
        if (nam == 'btn-newpackage') {
            if (packageForm.validate()) {
                if (parseFloat(packageForm.getInput('txt-mass').value)) {
                    registerNewPackage(packageForm.getInput('txt-package').value, parseFloat(packageForm.getInput('txt-mass').value));
                } else {
                    dhtmlx.message({text: 'Peso inválido'});
                }
            }
        }
    });
    
    theGrid = new dhtmlXGridObject('the_grid');
    theGrid.setImagesPath("css/imgs/");
    theGrid.setHeader("Presentación,Peso (en gramos)");
    theGrid.setInitWidths("*,*");
    theGrid.setColAlign("left,left");
    theGrid.setColTypes("ro,ro");
    theGrid.enableAutoHeight(true);
    theGrid.init();
    getAllPackages();
}

function registerNewPackage(name, mass) {
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/new_package');
    xhr.onload = function() {
        var r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
        if (r.code != -1) {
            getAllPackages();
        }
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    //xhr.send('name='+name+'&mass='+mass);
    console.log('name='+name+'&mass='+mass);
}

function getAllPackages() {
    var xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_packages');
    xhr.onload = function() {
        var packs = JSON.parse(xhr.responseText);
        var rows = [];
        for (var i in packs) {
            rows.push({id: packs[i].id, data: [packs[i].name, packs[i].mass]});
        }
        theGrid.clearAll();
        theGrid.parse({rows: rows}, 'json');
        theGrid.sortRows(0, 'str', 'asc');
    };
    xhr.send();
}

function deletePackage() {
    var packid = theGrid.getSelectedRowId();
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/delete_package');
    xhr.onload = function() {
        var r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
        if (r.code != -1) {
            getAllPackages();
        }
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('id='+packid);
}