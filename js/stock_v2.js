var theSidebar;
function doOnLoad() {
    theSidebar = new dhtmlXSideBar({
        parent: document.body,
        header: true,
        autohide: false,
        icons_path: "css/icons/",
        width: 200,
        offsets: {
            top: 6,
            left: 12,
            right: 12,
            bottom: 6
        },
        items: [
            {id: 'stock', text: 'Productos', icon: 'staroff.png', selected: true},
            {id: 'brand', text: 'Marcas', icon: 'staroff.png'},
            {id: 'package', text: 'Presentaciones', icon: 'staroff.png'},
            {type: 'separator'},
            {id: 'product', text: 'Nuevo Producto', icon: 'staroff.png'}
        ]
    });
    
    //Populate sidebar
    theSidebar.cells('stock').attachURL('viewstock_v2.html');
    theSidebar.cells('brand').attachURL('brand_v2.html');
    theSidebar.cells('package').attachURL('package_v2.html');
    theSidebar.cells('product').attachURL('product_v2.html');
    
    theSidebar.cells('stock').progressOn();
    setTimeout(function() {theSidebar.cells('stock').progressOff();}, 5000);
}