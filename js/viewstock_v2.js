var theDataview;
function doOnLoad() {
    var _selectable = false;
    if (parseInt(sessionStorage.privilege) == 1) {
        _selectable = true;
    }
    theDataview = new dhtmlXDataView({
        container:"products_container",
        select: _selectable,
        height: "auto",
        type:{
            template:"<img src='#image#' style='display: block; margin: auto; height:80px'><br><br><div style='text-align: center'><b>#name#</b></div>",
            height: 150
        }
    });
    theDataview.attachEvent('onItemClick', _onClick);
    getProductsFromServer();
}

function getProductsFromServer() {
    var xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_products');
    xhr.onload = function() {
        var products = JSON.parse(xhr.responseText).products;
        for (var i in products) {
            theDataview.add({
                id: products[i].id,
                name: products[i].name,
                image: "https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_picture?path="+products[i].picture1
            });
        }
        
    };
    xhr.send();
}

function _onClick(id, ev, html) {
    location.href = 'edit_product_v2.html';
    return true;
}