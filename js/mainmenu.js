var theSidebar;
function doOnLoad() {
    var _items;
    if (parseInt(sessionStorage.privilege) == 1) {
        _items = [
            {id: 'stock', text: 'Inventario', icon: 'staron.png'},
            {id: 'tickets', text: 'Compras', icon: 'staron.png'},
            {id: 'ads', text: 'Anuncios', icon: 'staron.png'},
            {id: 'coupons', text: 'Cupones y Descuentos', icon: 'staron.png'},
            {id: 'zipcodes', text: 'Códigos Postales', icon: 'staron.png'},
            {id: 'users', text: 'Cuentas de usuario', icon: 'staron.png'},
            {type: 'separator'},
            {id: 'logout', text: 'Cerrar sesión', icon: 'lock.png'}
        ];
    } else {
        _items = [
            {id: 'stock', text: 'Inventario', icon: 'staron.png', selected: true},
            {id: 'tickets', text: 'Compras', icon: 'staron.png'},
            {id: 'ads', text: 'Anuncios', icon: 'staron.png'},
            {id: 'coupons', text: 'Cupones y Descuentos', icon: 'staron.png'},
            {id: 'zipcodes', text: 'Códigos Postales', icon: 'staron.png'},
            {id: 'users', text: 'Cuentas de usuario', icon: 'staron.png'},
            {type: 'separator'},
            {id: 'logout', text: 'Cerrar sesión', icon: 'lock.png'}
        ];
    }
    theSidebar = new dhtmlXSideBar({
        parent: document.body,
        header: true,
        autohide: true,
        icons_path: "css/icons/",
        width: 200,
        offsets: {
            top: 6,
            left: 12,
            right: 12,
            bottom: 6
        },
        items: _items
    });
    theSidebar.attachHeader("my_logo");
    
    //Populate sidebar
    theSidebar.cells('stock').attachURL('stock_v2.html');
    theSidebar.cells('tickets').attachURL('ticket_v2.html');
    theSidebar.cells('ads').attachURL('ads_v2.html');
    theSidebar.cells('coupons').attachURL('coupon_v2.html');
    theSidebar.cells('zipcodes').attachURL('zipcodes_v2.html');
    theSidebar.cells('users').attachURL('admins_v2.html');
    
    if (parseInt(sessionStorage.privilege) == 1) {
        theSidebar.cells('coupons').attachURL('coupon_v2.html');
        theSidebar.cells('zipcodes').attachURL('zipcode_v2.html');
        theSidebar.cells('users').attachURL('user_v2.html');
    }
    
    //Logout
    /*
    theSidebar.attachEvent('onSelect', function(cellid) {
        if (cellid == 'logout') {
            var xhr = new XMLHttpRequest();
            xhr.open('post', 'https://www.touching-minds.com/main.py/apis/conta/dev/admin_logout');
            xhr.onload = function() {
                setTimeout(function() {
                    location.href = 'https://www.touching-minds.com/main.py/apis/conta/site/login.html';
                }, 1000);
                dhtmlx.message({
                    text: 'Sesión cerrada correctamente'
                });
            };
            xhr.send();
        }
    });
    */
}