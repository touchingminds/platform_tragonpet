var theGrid;
var theToolbar;

function doOnLoad() {
    theToolbar = new dhtmlXToolbarObject('the-toolbar');
    theGrid = new dhtmlXGridObject('the-grid');
    
    theGrid.setImagesPath("css/imgs/");
    theGrid.setHeader("Orden,Fecha,Usuario,Dirección,Producto,Total,Forma de pago");
    theGrid.setInitWidths("*,*,*,*,*,*,*");
    theGrid.setColAlign("left,left,left,left,left,left,left");
    theGrid.setColTypes("ron,ro,ro,ro,ro,ron,ro");
    theGrid.enableAutoHeight(true);
    theGrid.init();
    
    theToolbar.setIconsPath("css/imgs/");
    theToolbar.addButton('btn-view-ticket', 0, 'Ver Ticket', null, null);
    theToolbar.addButton('btn-delete-ticket', 1, 'Eliminar Ticket', null, null);
    theToolbar.attachEvent('onClick', function(elem_id) {
        if (elem_id == 'btn-delete-ticket') {
            if (theGrid.getSelectedRowId()) {
                dhtmlx.confirm({
                    title: 'Eliminar ticket',
                    text: '¿Realmente desea eliminar el ticket seleccionado?',
                    ok: 'Sí, eliminar',
                    cancel: 'No',
                    callback: function(res) {
                        if (res) {
                            deleteTicket();
                        }
                    }
                });
            } else {
                dhtmlx.message({
                    text: 'Seleccione el ticket que desea eliminar'
                });
            }
        }
        
        if (elem_id == 'btn-view-ticket') {
            if (theGrid.getSelectedRowId()) {
                openTicket();
            } else {
                dhtmlx.message({
                    text: 'Seleccione el ticket que desea visualizar'
                });
            }
        }
    });
    
    loadTicktes();
}

function deleteTicket() {
    var ticketid = theGrid.getSelectedRowId().split('_')[0];
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/delete_ticket');
    xhr.onload = function() {
        var r = JSON.parse(xhr.responseText);
        alert(r.message);
        if (r.code != -1) {
            theGrid.deleteSelectedRows();
            dhtmlx.alert({title: 'Ticket eliminado', text: r.message});
        }
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('id='+ticketid);
}

function openTicket() {
    var ticket = theGrid.getSelectedRowId().split('_');
    open('https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_ticket?id='+ticket[0]+'&auth='+ticket[1], '_blank');
}

function _getTicketData(ticket) {
    return new Promise(function(res, rej) {
        var xhr = new XMLHttpRequest();
        xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_ticket_data?id='+ticket.id);
        xhr.onload = function() {
            t = JSON.parse(xhr.responseText);
            t.id = ticket.id;
            t.auth = ticket.auth;
            res(t);
        };
        xhr.send();
    });
}

function loadTicktes() {
    var xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_tickets');
    xhr.onload = function() {
        var rows = [];
        var available_tickets = JSON.parse(xhr.responseText);
        var d = '';
        var total = 0;
        //_getAllTickets(available_tickets.reverse().slice(0, 5).reverse())
        
        for (var i in available_tickets) {
            _getTicketData(available_tickets[i])
            .then(function(ticket) {
                var tdata = [];
                d = ticket.purchase.date.split('-');
                
                //Order and Date
                tdata.push(ticket.id);
                tdata.push(d[2]+'/'+d[1]+'/'+d[0]);
                
                //User and Address
                tdata.push(ticket.user.name+' '+ticket.user.lastname);
                tdata.push(ticket.user.address+'. '+ticket.user.colony+', '+ticket.user.city);
                
                //Product and Total
                tdata.push('('+ticket.purchase.quantity+') '+ticket.product.brand+' '+ticket.product.name);
                var total = parseFloat(ticket.product.price * ticket.purchase.quantity);
                if (ticket.coupon.discount) {
                    if (ticket.coupon.percentage) {
                        tdata.push(parseFloat(total - (total * (ticket.coupon.discount / 100))).toFixed(2));
                    } else {
                        tdata.push(parseFloat(total - ticket.coupon.discount).toFixed(2));
                    }
                } else {
                    tdata.push(total.toFixed(2));
                }
                
                //Payment
                if (ticket.purchase.accredited) {
                    tdata.push('Tarjeta');
                } else {
                    tdata.push('Efectivo');
                }
                
                rows.push({id: ticket.id+'_'+ticket.auth, data: tdata});
                /*
                html += '<td><a class="pure-button button-secondary" href="https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_ticket?id='+ticket.id+'&auth='+ticket.auth+'" target="_blank"><i class="fa fa-file-pdf-o"></i></a></td>';
                if (parseInt(sessionStorage.privilege) == 1) {
                    html += '<button class="pure-button button-error" onclick="deleteTicket(this.id)" id="'+ticket.id+'">';
                } else {
                  html += '<button class="pure-button" disabled>';
                }
                */
                
            });
        }
        var iid = setInterval(function() {
            if (rows.length == available_tickets.length) {
                clearInterval(iid);
                theGrid.clearAll();
                theGrid.parse({rows: rows}, 'json');
                theGrid.setColSorting("int,na,str,na,na,na,na");
                theGrid.sortRows(0, 'int', 'des');
            }
        }, 500);
    };
    xhr.send();
}