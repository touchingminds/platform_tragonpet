var theDataview;
var theToolbar;

function doOnLoad() {
    var opts = [];
    opts.push([0,'obj','No aprobados',null]);
    opts.push([1,'obj','Aprobados',null]);
    opts.push([2,'obj','Administrador',null]);
    theToolbar = new dhtmlXToolbarObject('the-toolbar');
    theToolbar.setIconsPath("css/imgs/");
    theToolbar.addButtonSelect('sel-view-ad', 0, 'Ver Anuncios...', opts, null, null, true, true, 100, 'select');
    theToolbar.addButton('btn-approve-ad', 1, 'Aprobar Anuncio', null, null);
    theToolbar.addButton('btn-delete-ad', 2, 'Eliminar Anuncio', null, null);
    theToolbar.addButton('btn-new-ad', 3, 'Nuevo Anuncio', null, null);
    theToolbar.attachEvent('onClick', function(elem_id) {
        if (elem_id == 'btn-new-ad') {
            location.href = 'ad_v2.html';
        }
        
        if (elem_id == 'btn-approve-ad') {
            if (theDataview.getSelected()) {
                approveAd();
            } else {
                dhtmlx.message({
                    text: 'Seleccione alguna publicación para aprobar'
                });
            }
        }
        
        if (elem_id == 'btn-delete-ad') {
            if (theDataview.getSelected()) {
                dhtmlx.confirm({
                    title: 'Eliminar publicación',
                    text: '¿Realmente desea eliminar la publicación seleccionada?',
                    ok: 'Sí, eliminar',
                    cancel: 'No',
                    callback: function(res) {
                        if (res) {
                            if (theToolbar.getListOptionSelected('sel-view-ad') < 2) {
                                deleteAd();
                            } else {
                                deleteAdminAd();
                            }
                        }
                    }
                });
            } else {
                dhtmlx.message({
                    text: 'Seleccione el anuncio que desea eliminar'
                });
            }
        }
    });
    theToolbar.attachEvent('onButtonSelectHide', function(elem_id) {
        if (elem_id == 'sel-view-ad') {
            getAdsFromServer();
        }
    });
    
    theDataview = new dhtmlXDataView({
        container:"the-dataview",
        select: true,
        height: "auto",
        type:{
            template:"<img src='#image#' style='display: block; margin: auto; height:80px'><br><br><div style='text-align: center'><p><b>#title#</b></p><p>#category#</p></div>",
            height: 250
        },
        tooltip:{
            template:"<p><b>#content#<b></p>"
        }
    });
}

function deleteAd() {
    var adid = theDataview.getSelected();
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/delete_ad');
    xhr.onload = function() {
        var r = JSON.parse(xhr.responseText);
        console.log(r);
        theDataview.remove(adid);
        dhtmlx.alert({title: "Publicación eliminada", text: r.message});
        
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('id='+adid);
}

function deleteAdminAd() {
    var adid = theDataview.getSelected();
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/delete_admin_ad');
    xhr.onload = function() {
        var r = JSON.parse(xhr.responseText);
        console.log(r);
        theDataview.remove(adid);
        dhtmlx.alert({title: "Publicación eliminada", text: r.message});
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('id='+adid);
}

function approveAd() {
    var adid = theDataview.getSelected();
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/approve_ad');
    xhr.onload = function() {
        var r = JSON.parse(xhr.responseText);
        console.log(r);
        theDataview.remove(adid);
        dhtmlx.alert({title: "Aprobado", text: "El anuncio ha sido aprobado"});
    };
    xhr.onerror = reject;
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('id='+adid);
}

function getAdsFromServer() {
    var adtype = theToolbar.getListOptionSelected('sel-view-ad');
    var _url;
    switch (parseInt(adtype)) {
        case 0:
            _url = 'get_unapproved_ads';
            break;
        case 1:
            _url = 'get_all_approved_ads';
            break;
        case 2:
            _url = 'get_admin_ads';
            break;
    }
    var xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/'+_url);
    xhr.onload = function() {
        theDataview.clearAll();
        var ads = JSON.parse(xhr.responseText).ads;
        if (adtype < 2) {
            for (var i in ads) {
                _setAdsUser(ads[i]);
            }
        } else {
            for (var i in ads) {
                theDataview.add({
                    id: ads[i].id,
                    title: ads[i].title,
                    category: ads[i].category,
                    content: ads[i].content,
                    image: "https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_picture?path="+ads[i].picture
                });
            }
        }
        
        if (!parseInt(adtype) && ads.length) {
            theToolbar.enableItem('btn-approve-ad');
        } else {
            theToolbar.disableItem('btn-approve-ad');
        }
        
        if (!ads.length) {
            dhtmlx.alert({title: "Sin anuncios", text: "No hay anuncios por aprobar"});
        }
    };
    xhr.send();
}

function _setAdsUser(ad) {
    var xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_user_info?id='+ad.user);
    xhr.onload = function() {
        var user_info = JSON.parse(xhr.responseText);
        theDataview.add({
            id: ad.id,
            title: ad.category,
            content: ad.content,
            category: user_info.name+' '+user_info.lastname,
            image: "https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_picture?path="+ad.picture
        });
    };
    xhr.send();
}