var theLayout;
var theCarousel;
var theGrid;
var theToolbar, newPackagePopup, newPackageForm;
var product_id = 'DIACRG';
var product_packages = [];

function doOnLoad() {
    theLayout = new dhtmlXLayoutObject({
        parent: "product_layout",
        pattern: "3L",
        offsets: {
            top: 10,
            right: 20,
            bottom: 30,
            left: 20
        },
        cells: [
            {
                id: "a",
                header: false
            },
            {
                id: "b",
                header: false
            },
            {
                id: "c",
                header: false
            }
            
        ]
    });
    //Prepare Crousel
    theCarousel = theLayout.cells('b').attachCarousel({});
    theCarousel.setOffset(0, 0, 45);
    
    //Prepare Description
    theLayout.cells('c').attachObject('product_description');
    
    //Prepare Stock
    theToolbar = theLayout.cells('a').attachToolbar({});
    theToolbar.setIconsPath("css/imgs/");
    theToolbar.addButton('btn-back', 0, 'Regresar', null, null);
    theToolbar.addButton('popup-newpackage', 1, 'Agregar Presentación', null, null);
    theToolbar.addButton('btn-delete-package', 2, 'Eliminar Presentación', null, null);
    theToolbar.attachEvent('onClick', function(elem_id) {
        if (elem_id == 'btn-delete-package') {
            if (theGrid.getSelectedRowId()) {
                dhtmlx.confirm({
                    title: 'Eliminar presentación',
                    text: '¿Realmente desea eliminar la presentación seleccionada?',
                    ok: 'Sí, eliminar',
                    cancel: 'No',
                    callback: function(res) {
                        if (res) {
                            deletePackage();
                        }
                    }
                });
            } else {
                dhtmlx.message({
                    text: 'Seleccione la presentación que desea eliminar'
                });
            }
        } else if (elem_id == 'btn-back') {
            window.history.back();
        }
    });
    
    
    theGrid = theLayout.cells('a').attachGrid({});
    theGrid.setImagesPath("css/imgs/");
    theGrid.setHeader("Presentación,Disponibles,Precio");
    theGrid.setInitWidths("*,*,*");
    theGrid.setColAlign("left,left,left");
    theGrid.setColTypes("ro,dyn,price");
    theGrid.enableAutoHeight(true);
    theGrid.attachEvent('onEditCell', function(stage, rid, cind, nval, oval) {
        if (stage == 2) {
            console.log(stage, rid, cind, nval, oval);
            if (cind == 1 && (isNaN(parseInt(nval)) || parseInt(nval) < 0)) {
                dhtmlx.message({
                    text: 'Valor inválido'
                });
                return false;
            }
            
            if (cind == 2 && (isNaN(parseFloat(nval)) || parseFloat(nval) < 0)) {
                dhtmlx.message({
                    text: 'Valor inválido'
                });
                return false;
            }
            
            if (nval != oval) {
                updateStock(rid);
            }
            return true;
        }
    });
    theGrid.init();
    
    var xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_products');
    xhr.onload = function() {
        var products = JSON.parse(xhr.responseText).products;
        var cid;
        for (var i in products) {
            if (products[i].id == product_id) {
                document.getElementById('product_description').innerHTML = products[i].description;
                for (var k=1; k<6; k++) {
                    if (products[i]['picture'+k]) {
                        cid = theCarousel.addCell();
                        theCarousel.cells(cid).attachHTMLString("<div style='position: relative; left: 20%; top: 0px; overflow: auto; width: 100%; height: 100%;'>"+
                            "<img src='https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_picture?path="+products[i]['picture'+k]+"' border='0' style='width: 500px; height: 900px;' ondragstart='return false;'></div>");
                    }
                }
            }
        }
    };
    xhr.send();
    getProductPackages();
    getAllPackages();
}

function updateStock(packageid) {
    var price = parseFloat(theGrid.cells(packageid, 2).getValue());
    var stock = parseInt(theGrid.cells(packageid, 1).getValue());
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/update_product_stock');
    xhr.onload = function() {
        var r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
        getProductPackages();
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    //xhr.send('id='+product_id+'&package='+packageid+'&price='+price+'&quantity='+stock);
    console.log('id='+product_id+'&package='+packageid+'&price='+price+'&quantity='+stock);
}

function getProductPackages() {
    product_packages = [];
    var xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_product_packages?id='+product_id);
    xhr.onload = function() {
        var packages = JSON.parse(xhr.responseText);
        for (var i in packages) {
            if (i == (packages.length - 1)) {
                _setPackageStock({id: packages[i].id, name: packages[i].name}, false);
            } else {
                _setPackageStock({id: packages[i].id, name: packages[i].name}, true);
            }
        }
    };
    xhr.send();
}

function _setPackageStock(product_package, is_lastone) {
    var xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_product_stock?id='+product_id+'&package='+product_package.id);
    xhr.onload = function() {
        var r = JSON.parse(xhr.responseText);
        product_package.price = r.price;
        product_package.quantity = r.quantity;
        product_packages.push(product_package);
        
        if (is_lastone) {
            setTimeout(_setStockToGrid, 500);
        }
    };
    xhr.send();
}

function _setStockToGrid() {
    var rows = [];
    for (var i in product_packages) {
        rows.push({id: product_packages[i].id, data: [product_packages[i].name, product_packages[i].quantity, product_packages[i].price]});
    }
    theGrid.clearAll();
    theGrid.parse({rows: rows}, 'json');
    theGrid.sortRows(0, 'str', 'asc');
}

function getAllPackages() {
    var xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_packages');
    xhr.onload = function() {
          var packs = JSON.parse(xhr.responseText);
          var options = [];
          for (var i in packs) {
              options.push({value: packs[i].id, text: packs[i].name});
          }
          if (!options.length) {
              options.push({value: null, text: 'No existen presentaciones'});
          }
          newPackagePopup = new dhtmlXPopup({toolbar: theToolbar, id: 'popup-newpackage'});
          newPackageForm = newPackagePopup.attachForm([
              {type: 'select', label: 'Seleccione presentación', name: 'sel-package', labelWidth: 130, options: options, required: true},
              {type: 'button', name: 'btn-newpackage', value: 'Agregar'}
          ]);
          
          newPackageForm.attachEvent('onButtonClick', function(nam) {
              if (nam == 'btn-newpackage') {
                  if (newPackageForm.validate()) {
                      registerNewPackage(newPackageForm.getSelect('sel-package').value);
                  }
              }
          });
    };
    xhr.send();
}

function registerNewPackage(packageid) {
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/add_product_package');
    xhr.onload = function() {
        getProductPackages();
        var r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('package='+packageid+'&id='+product_id);
}

function deletePackage() {
    var packageid = theGrid.getSelectedRowId();
    var xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/delete_product_package');
    xhr.onload = function() {
        getProductPackages();
        var r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    //xhr.send('package='+packageid+'&id='+product_id);
    console.log('package='+packageid+'&id='+product_id);
}