let theGrid;
let theToolbar;
let brandPopup, brandForm;

function doOnLoad() {
    theToolbar = new dhtmlXToolbarObject('the_toolbar');
    theToolbar.setIconsPath("css/imgs/");
    theToolbar.addButton('popup-newbrand', 0, 'Agregar Marca', null, null);
    theToolbar.addButton('btn-delete-brand', 1, 'Eliminar Marca', null, null);
    theToolbar.attachEvent('onClick', function(elem_id) {
        if (elem_id == 'btn-delete-brand') {
            if (theGrid.getSelectedRowId()) {
                dhtmlx.confirm({
                    title: 'Eliminar marca',
                    text: '¿Realmente desea eliminar la marca seleccionada?',
                    ok: 'Sí, eliminar',
                    cancel: 'No',
                    callback: function(res) {
                        if (res) {
                            deleteBrand();
                        }
                    }
                });
            } else {
                dhtmlx.message({
                    text: 'Seleccione la marca que desea eliminar'
                });
            }
        }
    });
    
    brandPopup = new dhtmlXPopup({toolbar: theToolbar, id: 'popup-newbrand'});
    brandForm = brandPopup.attachForm([
        {type: 'input', label: 'Nombre', name: 'txt-brand', required: true},
        {type: 'button', name: 'btn-newbrand', value: 'Agregar'}
    ]);
    
    brandForm.attachEvent('onButtonClick', function(nam) {
        if (nam == 'btn-newbrand') {
            if (brandForm.validate()) {
                registerNewBrand(brandForm.getInput('txt-brand').value);
            }
        }
    });
    
    theGrid = new dhtmlXGridObject('the_grid');
    theGrid.setImagesPath("css/imgs/");
    theGrid.setHeader("Marca,Aplica descuento");
    theGrid.setInitWidths("*,*");
    theGrid.setColAlign("left,left");
    theGrid.setColTypes("ro,ch");
    theGrid.enableAutoHeight(true);
    theGrid.attachEvent("onCheck", function(rid,cind,checked){
        updateBrand(rid, +checked);
    });
    theGrid.init();
    getAllBrands();
}

function registerNewBrand(name) {
    let xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/new_brand');
    xhr.onload = function() {
        let r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
        if (r.code != -1) {
            getAllBrands();
        }
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('name='+name);
}

function getAllBrands() {
    let xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_brands');
    xhr.onload = function() {
        let packs = JSON.parse(xhr.responseText);
        let rows = [];
        for (let i in packs) {
            rows.push({id: packs[i].id, data: [packs[i].name, packs[i].dsc]});
        }
        theGrid.clearAll();
        theGrid.parse({rows: rows}, 'json');
        theGrid.sortRows(0, 'str', 'asc');
    };
    xhr.send();
}

function deleteBrand() {
    let brandid = theGrid.getSelectedRowId();
    let xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/delete_brand');
    xhr.onload = function() {
        let r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
        if (r.code != -1) {
            getAllBrands();
        }
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('id='+brandid);
}

function updateBrand(brandid, discountable) {
    let xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/update_brand');
    xhr.onload = function() {
        let r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
        if (r.code != -1) {
            getAllBrands();
        }
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('id='+brandid+'&dsc='+discountable);
}