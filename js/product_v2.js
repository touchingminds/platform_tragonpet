var theForm;
var theVault;

var formStructure = [
    {type:"settings", position:"label-left", inputWidth: 250},
    {type: "input", name: "productid", label: "ID:", tooltip: "Debe ser único", required: true},
    {type: "select", name:"brand", label:"Marca:", required: true},
    {type: "input", name:"name", label:"Nombre:", required: true},
    {type: "input", name:"description", label:"Descripción:", rows: 5, required: true},
    {type: "select", name:"petkind", label:"Alimento para:", required: true, options: [
        {value: "cat", text: "Gato"},
        {value: "dog", text: "Perro"}
    ]},
    {type: "select", name:"petage", label:"Edad:", required: true, options: [
        {value: "kitten", text: "Kitten"},
        {value: "adulto", text: "Adulto"}
    ]},
    {type: "select", name:"petrace", label:"Raza:", required: true, options: [
        {value: "small", text: "Pequeña"},
        {value: "large", text: "Mediana y Grande"},
        {value: "cat", text: "Gato"}
    ]},
    {type: "input", name:"diet", label:"Cantidad diaria recomendada (en gramos):", tooltip: "En gramos", required: true},
    {type: "multiselect", name:"package", label:"Presentaciones disponibles:", required: true},
    {type: "button", name: "btn", value: "Guardar"}
];

function doOnLoad() {
   theForm = new dhtmlXForm('the_form', formStructure);
   theVault = new dhtmlXVaultObject({
      container:  "the_vault",
      buttonUpload: false,
      buttonClear: false,
      autoStart: false,
      filesLimit: 5,
      multiple: true
   });
    
   //Events
   theForm.attachEvent("onChange", function (name, value){
       //console.log(name, value);
       if (name == 'petkind') {
           updateAge(value);
       }
   });
   
   theForm.attachEvent('onButtonClick', async function(name) {
      if (name == 'btn') {
         if (theForm.validate()) {
            if (!theVault.getStatus()) {
               dhtmlx.message({text: "Agregue al menos una imagen"});
            } else {
               await uploadProduct();
               //Set PID to uploader
               theVault.setURL('https://www.touching-minds.com/main.py/apis/tragonpet/dev/new_product_picture?pid='+theForm.getItemValue('productid'));
               theVault.upload();
            }
         }
      }
   });
    
    //Get brands & packages
    getAvailablePackages()
    .then(getAvailableBrands);
}

function uploadProduct() {
   return new Promise(function(resolve, reject) {
      
      let data = '';
      data += 'id='+theForm.getItemValue('productid');
      data += '&brand='+theForm.getItemValue('brand');
      data += '&name='+theForm.getItemValue('name');
      data += '&description='+theForm.getItemValue('description');
      data += '&petkind='+theForm.getItemValue('petkind');
      data += '&petrace='+theForm.getItemValue('petrace');
      data += '&petage='+theForm.getItemValue('petage');
      data += '&diet='+theForm.getItemValue('diet');
      data += '&available_packages='+theForm.getItemValue('package').join(',');
      
      let xhr = new XMLHttpRequest();
      xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/new_product');
      xhr.onload = function() {
         resolve();
      };
      xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
      //xhr.send(data);
      console.log('Would send');
      console.log(data);
      resolve();
   });
}


function getAvailablePackages() {
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/dev/get_packages');
        xhr.onload = function() {
            var packs = JSON.parse(xhr.responseText);
            var options = [];
            for (var i in packs) {
                options.push({text: packs[i].name, value: packs[i].id});
            }
            if (!options.length) {
                options.push({text: "No existen presentaciones", disabled: true});
            }
            theForm.reloadOptions('package', options);
            resolve();
        };
        xhr.send();
    });
}

function getAvailableBrands() {
    return new Promise(function(resolve, reject) {
        var xhr = new XMLHttpRequest();
        xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/dev/get_brands');
        xhr.onload = function() {
            var brands = JSON.parse(xhr.responseText);
            var options = [];
            for (var i in brands) {
                options.push({text: brands[i].name, value: brands[i].id});
            }
            if (!options.length) {
                options.push({text: "No existen marcas", disabled: true});
            }
            theForm.reloadOptions('brand', options);
            resolve();
        };
        xhr.send();
    });
}

function updateAge(kind) {
    var options = [];
    switch (kind) {
      case 'cat':
        options.push({value: "kitten", text: "Kitten"});
        options.push({value: "adulto", text: "Adulto"});
        break;
      case 'dog':
        options.push({value: "cachorro", text: "Cachorro"});
        options.push({value: "adulto", text: "Adulto"});
        options.push({value: "senior", text: "Senior"});
        break;
    }
    theForm.reloadOptions('petage', options);
}