var theGrid;
var theToolbar;
var zipcodePopup, zipcodeForm;

function doOnLoad() {
    var opts = [];
    opts.push(['general','obj','General',null]);
    opts.push(['bulk','obj','A Granel',null]);
    theToolbar = new dhtmlXToolbarObject('the_toolbar');
    theToolbar.setIconsPath("css/imgs/");
    theToolbar.addButtonSelect('sel-view-sale', 0, 'Tipo de venta', opts, null, null, true, true, 100, 'select');
    theToolbar.addButtonTwoState('btn-restrict', 1, 'Envíos restringidos', null, null);
    theToolbar.addButton('popup-new-zipcode', 2, 'Nuevo Código Postal', null, null);
    theToolbar.addButton('btn-delete-zipcode', 3, 'Eliminar Código Postal', null, null);
    theToolbar.attachEvent('onClick', function(elem_id) {
        
        if (elem_id == 'btn-delete-zipcode') {
            if (theGrid.getSelectedRowId()) {
                dhtmlx.confirm({
                    title: 'Eliminar código postal',
                    text: '¿Realmente desea eliminar el código postal seleccionado?',
                    ok: 'Sí, eliminar',
                    cancel: 'No',
                    callback: function(res) {
                        if (res) {
                            deleteZipcode();
                        }
                    }
                });
            } else {
                dhtmlx.message({
                    text: 'Seleccione el código postal que desea eliminar'
                });
            }
        }
    });
    theToolbar.attachEvent('onButtonSelectHide', function(elem_id) {
        if (elem_id == 'sel-view-sale' && theToolbar.getListOptionSelected('sel-view-sale')) {
            getZipcodes();
            getDeliveryRestriction();
        }
    });
    
    zipcodePopup = new dhtmlXPopup({toolbar: theToolbar, id: 'popup-new-zipcode'});
    zipcodeForm = zipcodePopup.attachForm([
        {type: "input", label: "Código Postal", name: "txt-zipcode", required: true},
        {type: "button", name: "btn-new-zipcode", value: "Agregar"}
    ]);
    
    zipcodeForm.attachEvent('onButtonClick', function(nam) {
        if (nam == 'btn-new-zipcode') {
            if (theToolbar.getListOptionSelected('sel-view-sale')) {
                if (zipcodeForm.validate()) {
                    if ((''+parseInt(zipcodeForm.getInput('txt-zipcode').value)).length == 5) {
                        newZipcode();
                    } else {
                        dhtmlx.message({text: 'Código Postal inválido'});
                    }
                }
            } else {
                dhtmlx.message({text: 'Elija un tipo de venta'});
            }
        }
    });
    
    theGrid = new dhtmlXGridObject('the_grid');
    theGrid.setImagesPath("css/imgs/");
    theGrid.setHeader("Código Postal");
    theGrid.setInitWidths("*");
    theGrid.setColAlign("left");
    theGrid.setColTypes("ro");
    theGrid.enableAutoHeight(true);
    theGrid.init();
}

function newZipcode() {
    let zipcode = zipcodeForm.getInput('txt-zipcode').value;
    let kind = theToolbar.getListOptionSelected('sel-view-sale');
    let xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/new_zipcode');
    xhr.onload = function() {
        let r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
        if (r.code != -1) {
            getZipcodes();
        }
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('zipcode='+zipcode+'&kind='+kind);
    console.log('zipcode='+zipcode+'&kind='+kind);
}

function getZipcodes() {
    let xhr = new XMLHttpRequest();
    let kind = theToolbar.getListOptionSelected('sel-view-sale');
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_zipcodes?kind='+kind);
    xhr.onload = function() {
        let zipcodes = JSON.parse(xhr.responseText).zipcodes;
        let rows = [];
        for (let i in zipcodes) {
            rows.push({id: zipcodes[i], data: [zipcodes[i]]});
        }
        theGrid.clearAll();
        theGrid.parse({rows: rows}, 'json');
        theGrid.sortRows(0, 'str', 'asc');
    };
    xhr.send();
}

function getDeliveryRestriction() {
    let kind = theToolbar.getListOptionSelected('sel-view-sale');
    let xhr = new XMLHttpRequest();
    xhr.open('get', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/get_delivery_restriction?kind='+kind);
    xhr.onload = function() {
        let restricted = JSON.parse(xhr.responseText).restricted;
        theToolbar.setItemState('btn-restrict', Boolean(parseInt(restricted)), false);
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send();
}

function deleteZipcode() {
    let zipcode = theGrid.getSelectedRowId();
    let kind = theToolbar.getListOptionSelected('sel-view-sale');
    let xhr = new XMLHttpRequest();
    xhr.open('post', 'https://www.touching-minds.com/main.py/apis/tragonpet/v2/delete_zipcode');
    xhr.onload = function() {
        let r = JSON.parse(xhr.responseText);
        dhtmlx.message({
            text: r.message
        });
        if (r.code != -1) {
            getZipcodes();
        }
    };
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
    xhr.send('zipcode='+zipcode+'&kind='+kind);
}